<?php

class Voo {
    
    private $codigo;
    private $origem;
    private $destino;
    
    public function getCodigo() {
        
        return $this->codigo;
        
    }

    public function getOrigem() {
        
        return $this->origem;
        
    }

    public function getDestino() {
        
        return $this->destino;
        
    }

    public function setCodigo($codigo) {
        
        $this->codigo = $codigo;
        
    }

    public function setOrigem($origem) {
        
        $this->origem = $origem;
        
    }

    public function setDestino($destino) {
        
        $this->destino = $destino;
        
    }


    
}
