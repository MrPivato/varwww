<?php

  class Retangulo {

    private $ladoMaior;
    private $ladoMenor;

    public function setLadoMaior($ladoMaior) {

        $this->ladoMaior = $ladoMaior;

    }

    public function setLadoMenor($ladoMenor) {

        $this->ladoMenor = $ladoMenor;

    }

    public function calculaArea() {

        return $this->ladoMaior * $this->ladoMenor;

    }

  }

?>
