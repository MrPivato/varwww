<?php

class Quadrado {

    private $lado;

    public function setLado($lado) {

        $this->lado = $lado;

    }

    public function calculaArea($lado) {

        return $this->lado * $this->lado;

    }

}

?>
