<?php

  class Pessoas {

      private $sexo;
      private $idade;

      public function getIdade() {

          return $this->idade;

      }

      public function getSexo() {

          return $this->sexo;

      }

      public function setIdade($idade) {

          $this->idade = $idade;

      }

      public function setSexo($sexo) {

          $this->sexo = $sexo;

      }

      public function oQueEh() {

        if ($this->sexo == 'M' && $this->idade < 18) {
            return '<p>Você é menino.</p>';
        }elseif ($this->sexo == 'M' && $this->idade >= 18) {
            return '<p>Você é homem.</p>';
        }elseif ($this->sexo == 'F' && $this->idade < 18) {
            return '<p>Você é menina.</p>';
        }elseif ($this->sexo == 'F' && $this->idade >= 18) {
            return '<p>Você é mulher.</p>';
        }

      }

  }

?>
