<?php

include_once '';

class Televisao {
    
    private $status;
    private $canal;
    private $volume;
    
    public function _construct() {
        
        $this->status = FALSE;
        
    }
    
    public function ligaDesliga() {
        
        if ($this->status == FALSE){
        
            $this->status = TRUE;
            $this->canal = 3;
            $this->volume = 10;
        
        } else {
            
            $this->status = FALSE;
        
        }
        
    }
    
    public function aumentaCanal() {
        
        $this->canal += 1;
        
    }
    
    public function diminuiCanal() {
        
        $this->canal -= 1;
        
    }
    
    public function aumentaVolume() {
        
        $this->volume += 1;
        
    }
    
    public function diminuiVolume() {
        
        $this->volume -= 1;
        
    }
    
    public function mostraCanal() {
        
        return $this->canal;
        
    }
    
    public function mostraVolume() {
        
        return $this->volume;
        
    }
    
}

?>
