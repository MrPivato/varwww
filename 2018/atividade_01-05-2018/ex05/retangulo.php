<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    <form action="retangulo.php" method="post">

      Informe a largura: <input type="number" name="largura"><br>

      <br>

      Informe a altura: <input type="number" name="altura"><br>

      <br>

      <input type="submit" name="enviar" value="Enviar">

    </form>

    <?php

      include_once '../ex04/retangulo.class.php';

      if(isset($_POST['enviar'])) {

          $retangulo = new Retangulo();

          $retangulo->setLadoMaior($_POST['largura']);
          $retangulo->setLadoMenor($_POST['altura']);

          echo "<hr>A área é: {$retangulo->calculaArea()}";
      }

    ?>

  </body>
</html>
