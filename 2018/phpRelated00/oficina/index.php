<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            
            include_once '/classes/automovel.class.php';
            include_once '/classes/esportivo.class.php';
            include_once '/classes/suv.class.php';
            
            $lamb = new Esportivo();
            $lamb->chassi = '12e3';
            $lamb->nome = 'Spider';
            $lamb->modelo = 'Spyder';
            $lamb->tem_chamas = 'sim';
            $lamb->andar();
            $lamb->frear();
            
            echo '<hr>';
            
            $jeep = new Suv();
            $jeep->chassi = '45jk';
            $jeep->nome = 'Jeep Massa';
            $jeep->modelo = 'Jipão';
            $jeep->tem_chamas = 'não';
            $jeep->andar();
            $jeep->frear();
        
        ?>
    </body>
</html>
