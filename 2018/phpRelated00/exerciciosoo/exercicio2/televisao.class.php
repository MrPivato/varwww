<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of televisao
 *
 * @author mauricio.rosito
 */
class Televisao {
    private $status;
    private $canal;
    private $volume;
    
    public function __construct() {
        $this->status = false;        
    }
    
    public function ligaDesliga(){
        //a TV estava desligada
        if($this->status==false){            
            $this->status = true;
            $this->canal = 3;
            $this->volume = 10;
        }else{ //a TV estava ligada
            $this->status = false;
        }
    }
    
    public function aumentaCanal(){
        $this->canal+=1;        
    }
    
    public function diminuiCanal(){
        $this->canal-=1;        
    }
    
    public function aumentaVolume(){
        $this->volume+=1;
    }
    
    public function diminuiVolume(){
        $this->volume-=1;
    }
    
    public function mostraCanal(){
        return $this->canal;
    }
    
    public function mostraVolume(){
       return $this->volume;
    }
}
