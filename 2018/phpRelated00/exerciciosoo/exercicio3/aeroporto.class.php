<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of aeroporto
 *
 * @author mauricio.rosito
 */
class Aeroporto {
    private $nome;
    private $cidade;
    public function getNome() {
        return $this->nome;
    }

    public function getCidade() {
        return $this->cidade;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }


}
