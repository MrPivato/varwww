<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of voo
 *
 * @author mauricio.rosito
 */
class Voo {
    private $codigo;
    private $origem;
    private $destino;
    
    public function getCodigo() {
        return $this->codigo;
    }

    public function getOrigem() {
        return $this->origem;
    }

    public function getDestino() {
        return $this->destino;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setOrigem($origem) {
        $this->origem = $origem;
    }

    public function setDestino($destino) {
        $this->destino = $destino;
    }


}
