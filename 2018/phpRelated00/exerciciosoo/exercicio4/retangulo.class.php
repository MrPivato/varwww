<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of retangulo
 *
 * @author mauricio.rosito
 */
class Retangulo {
    private $ladoMaior;
    private $ladoMenor;
    
    public function setLadoMaior($ladoMaior) {
        $this->ladoMaior = $ladoMaior;
    }

    public function setLadoMenor($ladoMenor) {
        $this->ladoMenor = $ladoMenor;
    }

    public function calculaArea(){
       return $this->ladoMaior * $this->ladoMenor;
    }

}
