<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Retângulo</title>
    </head>
    <body>
        <?php
            include_once '../exercicio4/retangulo.class.php';
            if(isset($_POST["botao"])){
                $retangulo = new Retangulo();
                $retangulo->setLadoMaior($_POST["ladomaior"]);
                $retangulo->setLadoMenor($_POST["ladomenor"]);
                echo "A área é: {$retangulo->calculaArea()}";
            }
        ?>
        <form name="formulario" method="post" action="retangulo.php">
            <p>Informe a lado maior: <input type="text" name="ladomaior"/></p>
            <p>Informe a lado menor: <input type="text" name="ladomenor"/></p>
            <p><input type="submit" name="botao" value="Calcula a área"/></p>
    
        </form>
            
    </body>
</html>
