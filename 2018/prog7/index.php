<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $a = 1;
        $b = 3;
        $c = 5;
        $res1 = ++$b - $a;
        $res2 = $c-- + $a;
        $res3 = --$a + $c++;
        echo "a = $a<br> b = $b<br> c = $c<br><br>";
        echo "res1 = $res1<br> res2 = $res2<br> res3 = $res3<br>";
        ?>
        <hr>
        <?php
        $soma = 0;
        $valor1 = 10;
        $valor2 = 20;
        $valor3 = 30;
        $soma += $valor1; // $soma fica com 10
        $soma += $valor2; // $soma fica com 10+20 = 30
        $soma *= $valor3; // $soma fica com 30*30 = 900
        $soma %= 100; // $soma fica com 900%100 = 0
        echo $soma;
        ?>
    </body>
</html>
