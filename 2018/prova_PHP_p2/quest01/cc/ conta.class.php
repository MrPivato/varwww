<?php

    abstract class Conta {


        //
        private $nbanco;
        private $nagencia;
        private $nconta;
        private $cpf;
        private $dataConta;
        private $saldo;


        // GETS
        public function getNumBanco(){
          return $this->nbanco;
        }
        public function getNumAgencia(){
          return $this->nagencia;
        }
        public function getNumConta(){
          return $this->nconta;
        }
        public function getCpf(){
          return $this->cpf;
        }
        public function getDataConta(){
          return $this->dataConta;
        }
        public function getSaldo(){
          return $this->saldo;
        }

        // SETS
        public function setNumBanco($nbanco){
          $this->nbanco = $nbanco;
        }
        public function setNumAgencia($nagencia){
          $this->nagencia = $nagencia;
        }
        public function setNumConta($nconta){
          $this->nconta = $nconta;
        }
        public function setCpf($cpf){
          $this->cpf = $cpf;
        }
        public function setDataConta($dataConta){
          $this->dataConta = $dataConta;
        }
        public function setSaldo($saldo){
          $this->saldo = $saldo;
        }

        /*
        public function sacar($valor){

          if ($valor > $this->saldo) {
            return false;
          }else {
            $this->saldo = $this->saldo - $valor;
          }
        }

      */
    }


?>
