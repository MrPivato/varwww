<?php

  require_once 'conta.class.php';

  class Standard extends Conta {

    public function sacar($valor){

      $juros = $valor * 0.1;
      $jurosCalc = $juros + $valor;

      if ($jurosCalc > $this->saldo) {

        return false;

      }else {

        $this->saldo -= $jurosCalc;

      }
    }

    public function depositar($valor)
    {
      $this->saldo += $valor;
    }

  }

?>
