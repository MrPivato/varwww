<?php
function f1($num = 1){
  echo $num." ";
  if ($num < 10) {
    f1(++$num);
    return;
  }
}
f1();
// 1 2 3 4 5 6 7 8 9 10 
?>
