<?php
function f3($n){
  if ($n == 0 || $n == 1) {
    return 1;
  }else {
    return (f3($n-1) + 1 * f3($n-2));
  }
}
echo f3(0).' - ';
echo f3(1).' - ';
echo f3(5);
// 1 - 1 - 8
?>
