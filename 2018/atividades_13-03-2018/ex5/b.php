<?php
function f2($n){
  if($n == 0){
    return 1;
  }else {
    return ($n * f2($n-1));
  }
}
echo f2(0)." - ";
echo f2(1)." - ";
echo f2(5);
// 1 - 1 - 120
?>
