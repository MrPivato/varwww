<?php
function f4($n){
  if ($n == 0) {
    return 1;
  }else {
    return f4($n-1)+f4($n-1);
  }
}
echo f4(1).' - ';
echo f4(3).' - ';
echo f4(5);
// 2 - 8 - 32
?>
