<?php

include_once ('animal.class.php');

class Gato extends Animal {
    
    private $cor_pelo;
    private $cor_olhos;

    //gets
    public function getCor_pelo() 
    {
        return $this->cor_pelo;
    }

    public function getCor_olhos() 
    {
        return $this->cor_olhos;
    }

    //sets
    public function setCor_pelo($cor_pelo) 
    {
        $this->cor_pelo = $cor_pelo;
    }

    public function setCor_olhos($cor_olhos) 
    {
        $this->cor_olhos = $cor_olhos;
    }    
}
