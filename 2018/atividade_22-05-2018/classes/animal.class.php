<?php

abstract class Animal {

   protected $proprietario;
   protected $nome;
   protected $idade;
   private $raca;
   private $pedigree;

   //gets
   public function getProprietario() 
   {
       return $this->proprietario;
   }

   public function getNome() 
   {
       return $this->nome;
   }

   public function getIdade() 
   {
       return $this->idade;
   }

   public function getRaca() 
   {
       return $this->raca;
   }

   public function getPedigree() 
   {
       return $this->pedigree;
   }

   //sets
   public function setProprietario($proprietario) 
   {
       $this->proprietario = $proprietario;
   }

   public function setNome($nome) 
   {
       $this->nome = $nome;
   }

   public function setIdade($idade) 
   {
       $this->idade = $idade;
   }

   public function setRaca($raca) 
   {
       $this->raca = $raca;
   }

   public function setPedigree($pedigree) 
   {
       $this->pedigree = $pedigree;
   }

}
