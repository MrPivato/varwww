<?php

include_once ('animal.class.php');

class Cachorro extends Animal{

    private $peso;
    private $temperamento;
    private $cor_pelo;

    //gets
    public function getPeso() 
    {
        return $this->peso;
    }

    public function getTemperamento() 
    {
        return $this->temperamento;
    }

    public function getCor_pelo() 
    {
        return $this->cor_pelo;
    }

    //sets
    public function setPeso($peso) 
    {
        $this->peso = $peso;
    }

    public function setTemperamento($temperamento) 
    {
        $this->temperamento = $temperamento;
    }

    public function setCor_pelo($cor_pelo) 
    {
        $this->cor_pelo = $cor_pelo;
    }
    
}
