<?php

include_once ('animal.class.php');

class Passarinho extends Animal{

    private $cor_asas;
    private $cor_corpo;
    private $asas_cortadas;
    
    //gets
    public function getCor_asas() 
    {
        return $this->cor_asas;
    }

    public function getCor_corpo() 
    {
        return $this->cor_corpo;
    }

    public function getAsas_cortadas() 
    {
        return $this->asas_cortadas;
    }

    //sets
    public function setCor_asas($cor_asas) 
    {
        $this->cor_asas = $cor_asas;
    }

    public function setCor_corpo($cor_corpo) 
    {
        $this->cor_corpo = $cor_corpo;
    }

    public function setAsas_cortadas($asas_cortadas) 
    {
        $this->asas_cortadas = $asas_cortadas;
    }
}
