<?php

class Proprietario {

    private $nome;
    private $cpf;
    private $rg;
    private $telefone;
    private $celular;
    private $endereco;
    private $cidade;
    private $cep;
    private $bairro;
    
    //gets
    public function getNome() 
    {
        return $this->nome;
    }

    public function getCpf() 
    {
        return $this->cpf;
    }

    public function getRg() 
    {
        return $this->rg;
    }

    public function getTelefone() 
    {
        return $this->telefone;
    }

    public function getCelular() 
    {
        return $this->celular;
    }

    public function getEndereco() 
    {
        return $this->endereco;
    }

    public function getCidade() 
    {
        return $this->cidade;
    }

    public function getCep() 
    {
        return $this->cep;
    }

    public function getBairro() 
    {
        return $this->bairro;
    }

    //sets
    public function setNome($nome) 
    {
        $this->nome = $nome;
    }

    public function setCpf($cpf) 
    {
        $this->cpf = $cpf;
    }

    public function setRg($rg) 
    {
        $this->rg = $rg;
    }

    public function setTelefone($telefone) 
    {
        $this->telefone = $telefone;
    }

    public function setCelular($celular) 
    {
        $this->celular = $celular;
    }

    public function setEndereco($endereco) 
    {
        $this->endereco = $endereco;
    }

    public function setCidade($cidade) 
    {
        $this->cidade = $cidade;
    }

    public function setCep($cep) 
    {
        $this->cep = $cep;
    }

    public function setBairro($bairro) 
    {
        $this->bairro = $bairro;
    }

}
