<?php
    include_once ('classes/proprietario.class.php');

    if (!empty($_POST)) {
   
        $proprietario = new Proprietario();
        
        $proprietario->setNome($_POST["nome"]);
        $proprietario->setCpf($_POST["cpf"]);
        $proprietario->setRg($_POST["rg"]);
        $proprietario->setTelefone($_POST["telefone"]);
        $proprietario->setCelular($_POST["celular"]);
        $proprietario->setEndereco($_POST["endereco"]);
        $proprietario->setCidade($_POST["cidade"]);
        $proprietario->setCep($_POST["cep"]);
        $proprietario->setBairro($_POST["bairro"]);
        
        var_dump($proprietario);
        echo '<hr>';
        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cadastro de Proprietários</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body>

        <form action="proprietario.php" method="post">
            
            <div class="flex">
                <label>Nome: </label>
                <input type="text" name="nome">
            </div>

            <div class="left">
                <label>CPF: </label>
                <input type="number" name="cpf">
            </div>

            <div class="right">
                <label>RG: </label>
                <input type="number" name="rg">
            </div>

            <br>

            <div class="left">
                <label>Telefone: </label>
                <input type="number" name="telefone">
            </div>

            <div class="right">
                <label>Celular: </label>
                <input type="number" name="celular">
            </div>

            <br clear="all">

            <div class="flex">
                <label>Endereço: </label>
                <input type="text" name="endereco">
            </div>

            <div class="left">
                <label>Cidade: </label>

                <select id="cidade" name="cidade">
                    <option value="" selected="selected" disabled>Selecione...</option>
                    <option value="CAX">Caxias do Sul</option>
                    <option value="BEN">Bento Gonçalves</option>
                    <option value="GAR">Garibaldi</option>
                </select>
            </div>

            <div class="right">
                <label>CEP: </label>
                <input type="number" name="cep">
            </div>

            <br clear="all">

            <div class="flex">
                <label>Bairro: </label>
                <input type="text" name="bairro">
            </div>

            <div class="right">
                <input type="submit" name="Enviar" value="Enviar">
            </div>

        </form>

    </body>
</html>
