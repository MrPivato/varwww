<?php  
	include_once ('classes/passarinho.class.php');

	if (!empty($_POST)) {
		
		$bird = new Passarinho();

		//da class Animal
		$bird->setProprietario($_POST['proprietario']);
		$bird->setNome($_POST['nome']);
		$bird->setIdade($_POST['idade']);
		//da class Passaro
		$bird->setRaca($_POST['raca']);
		$bird->setPedigree($_POST['pedigree']);
		$bird->setCor_asas($_POST['asas']);
		$bird->setCor_corpo($_POST['corpo']);
		$bird->setAsas_cortadas($_POST['cortadas']);

		var_dump($bird);
		echo '<hr>';

	}
?>
<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <title>Cadastro de animais</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
  </head>
  <body>
  	
    <form action="cadastroPassaro.php" method="post">
		
		<div class="flex">
			<label>Proprietário: </label>
			<select name="proprietario">
				<option value="Ana">Ana</option>
				<option value="Sakura">Sakura</option>
				<option value="Jose">Jose</option>
			</select>
		</div>

		<div class="flex">
			<label>Nome: </label>
			<input type="text" name="nome">
		</div>

		<div class="left">
			<label>Tipo: </label>
			<select name="animal" disabled>
				<option value="passaro">Passaro</option>
				<option value="cachorro">Cachorro</option>
				<option value="gato">Gato</option>
			</select>
		</div>

		<div class="right">
			<label>Idade: </label>
			<input type="number" name="idade">
		</div>

		<br clear="all">

		<fieldset>
			<legend>Passaro</legend>

			<div class="left">
				<label>Raça: </label>
				<select name="raca">
					<option value="CA">Canarinho</option>
					<option value="UB">Urubu</option>
				</select>
			</div>

			<div class="right">
				<label>Pedigree: </label>
				<select name="pedigree">
					<option value="Sim">Sim</option>
					<option value="Nao">Não</option>
				</select>
			</div>

			<br clear="all">

			<div class="left">
				<label>Cor das Asas: </label>
				<input type="text" name="asas">
			</div>

			<div class="right">
				<label>Cor do corpo</label>
				<input type="text" name="corpo">
			</div>

			<br clear="all">

			<div class="flex">
				<label>Asas cortadas: </label>
				<select name="cortadas">
					<option value="Sim">Sim</option>
					<option value="Nao">Não</option>
				</select>
			</div>

		</fieldset>

		<div class="right">
                <input type="submit" name="Enviar" value="Enviar">
		</div>

	</form>

  </body>
</html>