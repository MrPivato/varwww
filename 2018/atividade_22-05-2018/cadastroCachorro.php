<?php  
	include_once ('classes/cachorro.class.php');

	if (!empty($_POST)) {
		
		$dog = new Cachorro();

		//da class Animal
		$dog->setProprietario($_POST['proprietario']);
		$dog->setNome($_POST['nome']);
		$dog->setIdade($_POST['idade']);
		//da class Cachorro
		$dog->setRaca($_POST['raca']);
		$dog->setPeso($_POST['peso']);
		$dog->setPedigree($_POST['pedigree']);
		$dog->setTemperamento($_POST['temperamento']);
		$dog->setCor_pelo($_POST['pelo']);

		var_dump($dog);
		echo '<hr>';

	}
?>

<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <title>Cadastro de animais</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
  </head>
  <body>
    <?php

    ?>

    <form action="cadastroCachorro.php" method="post">
		
		<div class="flex">
			<label>Proprietário: </label>
			<select name="proprietario">
				<option value="Ana">Ana</option>
				<option value="Sakura">Sakura</option>
				<option value="Jose">Jose</option>
			</select>
		</div>

		<div class="flex">
			<label>Nome: </label>
			<input type="text" name="nome">
		</div>

		<div class="left">
			<label>Tipo: </label>
			<select name="animal" disabled>
				<option value="cachorro">Cachorro</option>
				<option value="gato">Gato</option>
				<option value="passaro">Passaro</option>
			</select>
		</div>

		<div class="right">
			<label>Idade: </label>
			<input type="number" name="idade">
		</div>

		<br clear="all">

		<fieldset>
			<legend>Cachorro: </legend>

			<div class="left">
				<label>Raça: </label>
				<select name="raca">
					<option value="PA">Pastor Alemão</option>
					<option value="VL">Vira Lata</option>
				</select>
			</div>

			<div class="right">
				<label>Peso: </label>
				<input type="number" name="peso">
			</div>

			<br clear="all">

			<div class="left">
				<label>Pedigree: </label>
				<select name="pedigree">
					<option value="Sim">Sim</option>
					<option value="Nao">Não</option>
				</select>
			</div>

			<div class="right">
				<label>Temperamento: </label>
				<select name="temperamento">
					<option value="Calmo">Calmo</option>
					<option value="Bravo">Bravo</option>
				</select>
			</div>

			<br clear="all">

			<div class="flex">
				<label>Cor do Pelo: </label>
				<input type="text" name="pelo">
			</div>

		</fieldset>

		<div class="right">
            <input type="submit" name="Enviar" value="Enviar">
		</div>

	</form>

  </body>
</html>
