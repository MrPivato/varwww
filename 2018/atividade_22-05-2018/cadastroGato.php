<?php  
	include_once ('classes/gato.class.php');

	if (!empty($_POST)) {
		
		$cat = new Gato();

		//da class Animal
		$cat->setProprietario($_POST['proprietario']);
		$cat->setNome($_POST['nome']);
		$cat->setIdade($_POST['idade']);
		//da class Gato
		$cat->setRaca($_POST['raca']);
		$cat->setPedigree($_POST['pedigree']);
		$cat->setCor_pelo($_POST['pelo']);
		$cat->setCor_olhos($_POST['olhos']);

		var_dump($cat);
		echo '<hr>';

	}
?>
<!DOCTYPE html>
<html lang="">
  <head>
    <meta charset="utf-8">
    <title>Cadastro de animais</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
  </head>
  <body>
  	
    <form action="cadastroGato.php" method="post">
		
		<div class="flex">
			<label>Proprietário: </label>
			<select name="proprietario">
				<option value="Ana">Ana</option>
				<option value="Sakura">Sakura</option>
				<option value="Jose">Jose</option>
			</select>
		</div>

		<div class="flex">
			<label>Nome: </label>
			<input type="text" name="nome">
		</div>

		<div class="left">
			<label>Tipo: </label>
			<select name="animal" disabled>
				<option value="gato">Gato</option>
				<option value="cachorro">Cachorro</option>
				<option value="passaro">Passaro</option>
			</select>
		</div>

		<div class="right">
			<label>Idade: </label>
			<input type="number" name="idade">
		</div>

		<br clear="all">

		<fieldset>
			<legend>Gato</legend>

			<label>Raça: </label>
			<div class="left">
				<select name="raca">
					<option value="S">Siamês</option>
					<option value="VL">Vira Lata</option>
				</select>
			</div>

			<div class="right">
				<label>Pedigree: </label>
				<select name="pedigree">
					<option value="Sim">Sim</option>
					<option value="Nao">Não</option>
				</select>
			</div>

			<br clear="all">

			<div class="left">
				<label>Cor do pelo: </label>
				<input type="text" name="pelo">
			</div>

			<div class="right">

				<label>Cor dos olhos: </label>
				<input type="text" name="olhos">
				
			</div>

		</fieldset>

		<div class="right">
            <input type="submit" name="Enviar" value="Enviar">
		</div>


	</form>

  </body>
</html>