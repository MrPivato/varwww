<?php

include_once 'automovel.class.php';

class Esportivo extends Automovel{
    
    public $tem_chamas;

    public function frear() {
        
        echo '<p>Freiar um esportivo</p>';
        
    }

    private function maisPotencia(){
        
        echo '<p>Mais potência</p>';
        
    }
            
    public function andar() {
        
        parent::andar();
        $this->maisPotencia();
        
    }
            
}
