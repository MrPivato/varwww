<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // put your code here
        ?>
        <hr>
        <ul>
            <li>Primeiro item da lista</li>
            <li>
                Segundo item da lista:
                <ul>
                    <li>Primeiro item da lista aninhada</li>
                    <li>Segundo item da lista aninhada</li>
                </ul>
            </li>
            <li>Terceiro item da lista</li>
        </ul>
        <hr>
        <ol>
            <li>Primeiro item da lista</li>
            <li>Segundo item da lista</li>
            <li>Terceiro item da lista</li>
            <li>Quarto item da lista</li>
            <li>Quinto item da lista</li>
        </ol>
        <hr>
        <dl>
            <dt>HTML</dt>
            <dd>
                HTML é a linguagem de marcação de textos utilizada para exibir textos 
                como páginas na Internet. 
            </dd>
            <dt>Navegador</dt>
            <dd>
                Navegador é o software que requisita um documento HTML através do 
                protocolo HTTP e exibe seu conteúdo em uma janela.
            </dd>
        </dl>
        <hr>
    </body>
</html>
