# Esse arquivo contém exemplos de consultas (SELECT) em SQL
# Execute passo-a-passo e observe os resultados

### SETUP
# criando um DB específico para a demo
CREATE DATABASE demo_select;
USE demo_select;

CREATE TABLE pessoas (
    id INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(20),
    sobrenome VARCHAR(30) NOT NULL,
    idade INT,
    cidade VARCHAR(30),
    PRIMARY KEY (id)
);

# populando as tabelas (não é necessário colocar 
# os valores de campos com auto_increment)
INSERT INTO pessoas (nome, sobrenome, idade, cidade) VALUES ('eu', 'meu sobrenome', 13, 'minha cidade');
INSERT INTO pessoas (nome, sobrenome, idade, cidade) VALUES ('tu', 'teu sobrenome', 20, 'tua cidade');
INSERT INTO pessoas (nome, sobrenome, idade, cidade) VALUES ('ele', 'sobrenome dele', 27, 'cidade dele');
INSERT INTO pessoas (nome, sobrenome, idade, cidade) VALUES ('ela', 'sobrenome dela', 34, 'cidade dela');

SELECT 
    *
FROM
    pessoas;

SELECT 
    nome, sobrenome
FROM
    pessoas;

SELECT 
    nome AS n, sobrenome AS s
FROM
    pessoas;

# distinct remove resultados duplicados
# no exemplo abaixo, temos duas pessoas com a mesma cidade
# ex.: em quais cidades as pessoas desse banco de dados moram?
INSERT INTO pessoas (nome, sobrenome, idade, cidade) VALUES ('outra ela', 'outro sobrenome dela', 41, 'cidade dela');
SELECT DISTINCT
    cidade
FROM
    pessoas;

SELECT DISTINCT
    sobrenome, cidade
FROM
    pessoas;

SELECT 
    nome
FROM
    pessoas
WHERE
    cidade = 'minha cidade';

SELECT 
    nome
FROM
    pessoas
WHERE
    idade = 34;

SELECT 
    nome
FROM
    pessoas
WHERE
    idade = '34';

SELECT 
    nome
FROM
    pessoas
WHERE
    idade > 13;

SELECT 
    nome
FROM
    pessoas
WHERE
    idade <> 34;

SELECT 
    nome
FROM
    pessoas
WHERE
    idade BETWEEN 20 AND 40;

SELECT 
    nome
FROM
    pessoas
WHERE
    nome BETWEEN 'a' AND 'f';

SELECT 
    nome
FROM
    pessoas
WHERE
    nome BETWEEN 'ela' AND 'elu';

SELECT 
    nome
FROM
    pessoas
WHERE
    idade IN (13 , 34);

SELECT 
    nome
FROM
    pessoas
WHERE
    nome LIKE 'e%';

SELECT 
    nome
FROM
    pessoas
WHERE
    nome LIKE '_l_';

SELECT 
    nome
FROM
    pessoas
WHERE
    (idade BETWEEN 20 AND 40)
        AND (nome LIKE 'e%');

SELECT 
    nome, idade
FROM
    pessoas
ORDER BY idade DESC;

SELECT 
    nome, idade, cidade
FROM
    pessoas
WHERE
    idade BETWEEN 20 AND 40
ORDER BY cidade ASC;

SELECT 
    *
FROM
    pessoas
ORDER BY cidade ASC , idade DESC;

SELECT 
    *
FROM
    pessoas
LIMIT 2;

SELECT 
    *
FROM
    pessoas
LIMIT 2 , 1;

SELECT 
    AVG(idade)
FROM
    pessoas;

SELECT 
    AVG(idade), cidade
FROM
    pessoas
GROUP BY cidade;

SELECT 
    COUNT(idade)
FROM
    pessoas
WHERE
    idade BETWEEN 20 AND 40;

SELECT 
    COUNT(idade), cidade
FROM
    pessoas
WHERE
    idade BETWEEN 10 AND 50
GROUP BY cidade;

SELECT 
    SUM(idade)
FROM
    pessoas;

SELECT 
    SUM(idade) / 4
FROM
    pessoas;

SELECT 
    idade / 4 + 2
FROM
    pessoas;

CREATE TABLE produtos (
    produto VARCHAR(20),
    preco INT,
    produtor VARCHAR(20)
);

INSERT INTO produtos VALUES ('cama', 400, 'produtor1');
INSERT INTO produtos VALUES ('mesa', 200, 'produtor2');
INSERT INTO produtos VALUES ('cadeira', 100, 'produtor2');
INSERT INTO produtos VALUES ('armário', 600, 'produtor1');

SELECT 
    produtor, SUM(preco) AS total
FROM
    produtos
GROUP BY (produtor);

SELECT 
    produtor, SUM(preco) AS total
FROM
    produtos
GROUP BY (produtor)
HAVING total > 500;

### JOINS:
# sempre que precisar de informações que estão em múltiplas tabelas
DROP TABLE pessoas;
DROP TABLE produtos;

CREATE TABLE funcionarios (
    id_func INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(30),
    salario INT,
    PRIMARY KEY (id_func)
);

CREATE TABLE dependentes (
    id_dep INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(30),
    auxilio INT,
    id_func INT,
    PRIMARY KEY (id_dep),
    FOREIGN KEY (id_func)
        REFERENCES funcionarios (id_func)
);

INSERT INTO funcionarios (nome, salario) VALUES ('mario', 1000); #id 1
INSERT INTO funcionarios (nome, salario) VALUES ('carlos', 2000); #id 2
INSERT INTO funcionarios (nome, salario) VALUES ('bruna', 3000); #id 3

INSERT INTO dependentes (nome, auxilio, id_func) VALUES ('carlinhos', 100, 2);
INSERT INTO dependentes (nome, auxilio, id_func) VALUES ('bruninha', 200, 3);
INSERT INTO dependentes (nome, auxilio, id_func) VALUES ('carlinha', 100, 2);

SELECT 
    *
FROM
    funcionarios,
    dependentes;

SELECT 
    nome
FROM
    funcionarios,
    dependentes;

SELECT 
    f.nome
FROM
    funcionarios AS f,
    dependentes AS d;

SELECT 
    f.nome, d.nome
FROM
    funcionarios AS f,
    dependentes AS d
WHERE
    f.id_func = d.id_func;

SELECT 
    f.nome, d.nome
FROM
    funcionarios AS f
        INNER JOIN
    dependentes AS d ON f.id_func = d.id_func;

SELECT 
    funcionarios.nome, dependentes.nome
FROM
    funcionarios
        INNER JOIN
    dependentes ON funcionarios.id_func = dependentes.id_func;

SELECT 
    f.nome, d.nome
FROM
    funcionarios AS f
        LEFT JOIN
    dependentes AS d ON f.id_func = d.id_func;

SELECT 
    f.nome, COUNT(d.nome)
FROM
    funcionarios AS f
        LEFT JOIN
    dependentes AS d ON f.id_func = d.id_func
GROUP BY f.nome;
    
SELECT 
    *
FROM
    funcionarios
        LEFT JOIN
    dependentes ON funcionarios.id_func = dependentes.id_func;


# right outer join é o contrário, veja abaixo
INSERT INTO dependentes (nome, auxilio, id_func) VALUES ('joaozinho', 100, NULL);
INSERT INTO dependentes (nome, auxilio, id_func) VALUES ('mariazinha', 1000, NULL);

SELECT 
    d.nome, f.nome
FROM
    funcionarios AS f
        RIGHT JOIN
    dependentes AS d ON f.id_func = d.id_func;
    
SELECT 
    *
FROM
    funcionarios
        LEFT JOIN
    dependentes ON funcionarios.id_func = dependentes.id_func;

CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    dia DATE,
    PRIMARY KEY (id)
);

INSERT INTO posts (dia) VALUES ('2017-10-22');
INSERT INTO posts (dia) VALUES ('2016-11-09');

CREATE TABLE reacao (
    id INT NOT NULL AUTO_INCREMENT,
    tipo CHAR(10),
    PRIMARY KEY (id)
);

INSERT INTO reacao (tipo) VALUES ('like');
INSERT INTO reacao (tipo) VALUES ('amou');
INSERT INTO reacao (tipo) VALUES ('chorou');

CREATE TABLE posts_curtidas (
    id_post INT,
    id_reacao INT,
    FOREIGN KEY (id_post)
        REFERENCES posts (id),
    FOREIGN KEY (id_reacao)
        REFERENCES reacao (id)
);

INSERT INTO posts_curtidas VALUES (1, 1);
INSERT INTO posts_curtidas VALUES (1, 2);
INSERT INTO posts_curtidas VALUES (2, 2);
INSERT INTO posts_curtidas VALUES (2, 3);

SELECT 
    p.id, r.tipo
FROM
    posts AS p,
    posts_curtidas AS pc,
    reacao AS r
WHERE
    p.id = pc.id_post
        AND pc.id_reacao = r.id;

SELECT 
    *
FROM
    posts AS p,
    posts_curtidas AS pc,
    reacao AS r;

SELECT 
    p.id, r.tipo
FROM
    posts AS p
        INNER JOIN
    posts_curtidas AS pc
        INNER JOIN
    reacao AS r ON p.id = pc.id_post
        AND pc.id_reacao = r.id;

SELECT 
    nome
FROM
    funcionarios
WHERE
    salario > 2000 
UNION SELECT 
    nome
FROM
    dependentes
WHERE
    auxilio > 100;


CREATE TABLE nomes (
    nome VARCHAR(30)
);

INSERT INTO nomes
	SELECT nome FROM funcionarios WHERE salario > 2000;

SELECT 
    *
FROM
    nomes;

SELECT 
    nome
FROM
    funcionarios
WHERE
    salario = (SELECT 
            auxilio
        FROM
            dependentes
        WHERE
            nome = 'mariazinha');

SELECT 
    nome
FROM
    funcionarios
WHERE
    salario < (SELECT 
            auxilio
        FROM
            dependentes
        WHERE
            nome LIKE 'car%');
    
SELECT 
    nome,
    salario AS sal,
    (SELECT 
            nome
        FROM
            funcionarios
        WHERE
            salario < sal
        ORDER BY salario ASC
        LIMIT 1) AS menor_salario1,
    (SELECT 
            nome
        FROM
            funcionarios
        WHERE
            salario < sal
        ORDER BY salario ASC
        LIMIT 1 , 1) AS menor_salario2
FROM
    funcionarios;

INSERT INTO funcionarios (nome, salario) VALUES ('pedro', 4000);
SELECT 
    *
FROM
    dependentes;

SELECT 
    f.nome, f.salario / totals.total AS '% do total'
FROM
    funcionarios AS f,
    (SELECT 
        SUM(salario) AS total
    FROM
        funcionarios) AS totals
WHERE
    f.id_func NOT IN (SELECT 
            d.id_func
        FROM
            dependentes AS d
        WHERE
            d.id_func IS NOT NULL);
