CREATE DATABASE aluno_netshoes_data;
USE aluno_netshoes_data;
CREATE TABLE user_data(
    id1 INT,
    username VARCHAR(30),
    nome1 VARCHAR(100),
    sobrenome VARCHAR(100),
    cpf CHAR(14),
    rg CHAR(10),
    data_nascimento DATE,
    sexo CHAR(1),
    senha VARCHAR(200),
    credit_card VARCHAR(30),
    pais VARCHAR(80),
    estado VARCHAR(80),
    cidade VARCHAR(80),
    bairro VARCHAR(80),
    rua VARCHAR(80),
    casa INT,
    cep CHAR(9),
    PRIMARY KEY (id1)
);
CREATE TABLE produto(
    id2 INT,
    tamanho FLOAT,
    modelo VARCHAR(30),
    custo FLOAT,
    nome2 VARCHAR(150),
    cor VARCHAR(20),
    cat1 VARCHAR(20),
    peso FLOAT,
    PRIMARY KEY (id2)
);
CREATE TABLE categoria(
    id3 INT,
    cat2 VARCHAR(20),
    id_produto INT,
    sobre VARCHAR(255),
    PRIMARY KEY (id3),
    FOREIGN KEY (id_produto) REFERENCES produto(id2)
);
CREATE TABLE endereco(
    id INT,
    id_pessoa INT,
    pais VARCHAR(80),
    estado VARCHAR(80),
    cidade VARCHAR(80),
    bairro VARCHAR(80),
    rua VARCHAR(80),
    casa INT,
    cep CHAR(9),
    PRIMARY KEY (id),
    FOREIGN KEY (id_pessoa) REFERENCES user_data(id1)
);
CREATE TABLE compra(
	id INT,
    #
    id_pessoa INT,
    pais VARCHAR(80),
    estado VARCHAR(80),
    cidade VARCHAR(80),
    bairro VARCHAR(80),
    rua VARCHAR(80),
    casa INT,
    cep CHAR(9),
    #
    id_produto INT,
    tamanho_t FLOAT,
    modelo VARCHAR(30),
    custo FLOAT,
    nome VARCHAR(150),
    cor VARCHAR(20),
    cat_p VARCHAR(20),
    peso FLOAT,
    #
    id_categoria INT,
    cat_c VARCHAR(20),
    #
    PRIMARY KEY (id),
    FOREIGN KEY (id_pessoa) REFERENCES user_data(id1),
    FOREIGN KEY (id_produto) REFERENCES produto(id2),
    FOREIGN KEY (id_categoria) REFERENCES categoria(id3)
);
#-------------------------------------------------------------------------------------------
# insert a1
INSERT INTO user_data  VALUES (1, "nissim_foobar", "Nissim", "Ourfali", "111.111.111-11",
"11.111.111", "1920-12-10", "M", "vouparaaabaleia123", "123.456.78-9", "Brasil", "RJ",
"Rio de Janeiro", "Favela", "Augusta", "1234", "99999-000" );
#insert a2
INSERT INTO user_data (id1, username, nome1, sobrenome, cpf, rg, data_nascimento,
sexo, senha, credit_card) VALUES (2, "maria_joao", "Maria", "Joao", "222.222.222-22",
"22.222.222", "1919-01-09", "F", "comunismonaoexiste444", "123.456.78-9");
#insert a3
INSERT INTO user_data (id1, username, nome1, sobrenome, cpf, rg, data_nascimento, senha, pais, estado, cidade,
bairro, rua, casa, cep) VALUES (3, "steve_wonds", "Steve", "Wonder", "333.333.333-33", "33.333.333", "1800-04-17",
"intel==back_door" ,"Brasil", "RS", "Porto Alegre", "Restinga", "Rua das casas", "1010", "44331-000" );
SELECT * FROM user_data;
#----------------------------------------------------------------------------------------------
# insert b1
INSERT INTO produto VALUES (1, 38.0, "Bota", 134.90, "Bota do Ben 10", "Verde", "bota", 1.5);
# insert b2
INSERT INTO produto (id2, tamanho, custo, nome2) VALUES (2, 35.7, 234.90, "Tênis pirateado");
# insert b3
INSERT INTO produto (id2, modelo, custo, nome2, peso)
VALUES (3, "Chinelo", 9999.90, "Chinelo mais pirateado ainda", 189.3);
SELECT * FROM produto;
#_____________________________________________________________________________________
# insert c1
INSERT INTO categoria (id3, cat2, id_produto) VALUES (1, "bota", 1);
# insert c2
INSERT INTO categoria (id3, cat2, id_produto) VALUES (2, "tenis", 2);
# insert c3
INSERT INTO categoria VALUES (3, "chinelo", 3, "Chinelo barato");
SELECT * FROM categoria;
#______________________________________________________________________________________
# update a1
UPDATE user_data SET senha = "esse_e_meu_bar_mitzvah_9090" WHERE username = "nissim_foobar";
# update a2
UPDATE user_data SET pais = "Cuba" WHERE id1 = 2;
SELECT * FROM user_data;
#--------------------------------------------------------------------------------------------
# update b1
UPDATE produto SET nome2 = "Bota do Ben 10, de qualidade" WHERE cor = "Verde";
# update b2
UPDATE produto SET tamanho = 43.8 WHERE nome2 = "Chinelo mais pirateado ainda";
SELECT * FROM produto;
#--------------------------------------------------------------------------------------------
# update c1
UPDATE categoria SET cat2 = "Chinelo" WHERE cat2 = "chinelo";
# update c2
UPDATE categoria SET cat2 = "Calçado" WHERE cat2 = "tenis";
SELECT * FROM categoria;
#--------------------------------------------------------------------------------------------
# delete a1
DELETE FROM user_data WHERE username = "steve_wonds";
SELECT * FROM user_data;
# delete c1
DELETE FROM categoria WHERE cat2 = "bota";
SELECT * FROM categoria;
#_______________________________________________________________________________________________
#insert a1
INSERT INTO endereco (id, id_pessoa, pais, estado, cidade, bairro, rua, casa, cep)
SELECT 1, id1, pais, estado, cidade, bairro, rua, casa, cep FROM user_data WHERE id1 = 1;
#insert a2
INSERT INTO endereco (id, id_pessoa, pais, estado, cidade, bairro, rua, casa, cep)
SELECT 2, id1, pais, estado, cidade, bairro, rua, casa, cep FROM user_data WHERE id1 = 2;
SELECT * FROM endereco;
#_______________________________________________________________________________________________
#id ,id_pessoa ,pais ,estado ,cidade ,bairro ,rua ,casa ,cep ,id_produto ,tamanho_t ,modelo ,custo ,nome ,cor ,cat_p ,peso ,id_categoria , cat_c
#insert b1
/*
#INSERT INTO compra (id, id_pessoa ,pais ,estado ,cidade ,bairro ,rua ,casa ,cep ,id_produto ,
#tamanho_t ,modelo ,custo ,nome ,cor ,cat_p ,peso ,id_categoria , cat_c) 
#SELECT 1, id1 ,pais ,estado ,cidade ,bairro ,rua ,casa ,cep ,id2 ,
#tamanho ,modelo ,custo ,nome2 ,cor ,cat1 ,peso ,id3 , cat2 FROM (user_data, produto, categoria);
#INSERT INTO compra (id) VALUE (1);
#UPDATE compra SET  id_pessoa = user_data.id1 WHERE id = 1;
#SELECT * FROM compra;
	Durante o final de semana, me surgiu uma duvida, e não consegui muito bem juntar varias tabelas em uma só
    gostaria de esclarecer isto na aula
SELECT Orders.OrderID, Customers.CustomerName, Shippers.ShipperName
FROM ((Orders
INNER JOIN Customers ON Orders.CustomerID = Customers.CustomerID)
INNER JOIN Shippers ON Orders.ShipperID = Shippers.ShipperID);
*/
SELECT compra.id, user_data.id1, produto.id2
FROM ((compra
INNER JOIN user_data ON compra.id_pessoa = user_data.id1)
INNER JOIN produto ON compra.id_produto = produto.id2);
SELECT * FROM compra;
/* Não é armazenado no banco de dados:
    HTML do site
    CSS do site
    PHP do site
    Informações sigilosas
    Senhas, (Não deveria, mas estou armazenando)
*/
