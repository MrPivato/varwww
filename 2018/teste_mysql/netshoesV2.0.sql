/* Crie um banco de dados para armazenar alguns dados de um site de comércio 
(use o Netshoes como modelo). Você deverá criar tabelas para armazenar dados 
dos tipos mais variados, como produtos, categorias, clientes e etc. 
Tente criar o máximo de tabelas que puder, evitando criar tabelas que
armazenem dados que poderiam pertencer a várias entidades.
Tente definir da melhor maneira possível o tipo das colunas.
Indique também, de forma textual, dados que estão presentes na página
que não serão armazenados no banco de dados. */
CREATE DATABASE aluno_netshoes_data;
USE aluno_netshoes_data;
CREATE TABLE user_data(
	ID INT,
	username VARCHAR(30),
	nome VARCHAR(100),
	sobrenome VARCHAR(100),
	cpf CHAR(14),
	rg CHAR(10),
	data_nascimento DATE,
	sexo CHAR(1),
	senha VARCHAR(200),
	credit_card VARCHAR(30),
    pais VARCHAR(80),
    estado VARCHAR(80),
    cidade VARCHAR(80),
    bairro VARCHAR(80),
    rua VARCHAR(80),
    casa INT,
    cep CHAR(9)
);
CREATE TABLE produto(
	num INT,
	tamanho FLOAT,
	modelo VARCHAR(30),
	custo FLOAT,
	nome VARCHAR(150),
	cor VARCHAR(20),
	cat VARCHAR(20),
	peso FLOAT
);
CREATE TABLE categoria(
	cat VARCHAR(20)
);
/* Não é aramzenado no banco de dados:
	HTML do site
    CSS do site
    PHP do site
    Informações sigilosas
    Senhas
*/