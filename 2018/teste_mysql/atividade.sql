CREATE DATABASE aluno_hospital;
USE aluno_hospital;

CREATE TABLE ambulatorio(

	numeroA 	INT,
	andar 		INT,
	capacidade 	INT,
    
	PRIMARY KEY (numeroA)

);

CREATE TABLE medico(

	crm 			INT, 
	nome 			VARCHAR(255),
	idade 			INT,
	cidade 			VARCHAR(255),
	especialidade 	VARCHAR(255),
	numeroA 		INT,
    
	PRIMARY KEY (crm),
    
	FOREIGN KEY (numeroA) REFERENCES ambulatorio (numeroA)

);

CREATE TABLE paciente(

	rg 		INT,
	nome 	VARCHAR(255),
	idade 	INT,
	cidade 	VARCHAR(255),
	doenca 	VARCHAR(255),
    
	PRIMARY KEY (rg)

);

CREATE TABLE consulta(

	crm 	INT,
	rg 		INT,
	dia 	DATE,
	hora 	TIME,
    
	FOREIGN KEY (crm) REFERENCES medico (crm),
	FOREIGN KEY (rg) REFERENCES paciente (rg)

);

CREATE TABLE funcionario(

	rg 			INT,
    nome 		VARCHAR(255),
    idade 		INT,
    cidade 		VARCHAR(255),
    salario 	FLOAT,
    
    PRIMARY KEY (rg)
    
);

#Inserts:

INSERT INTO ambulatorio	VALUES (0,	1, 	80);
INSERT INTO ambulatorio VALUES (1, 	1, 	50);
INSERT INTO ambulatorio	VALUES (2, 	1, 	30);
INSERT INTO ambulatorio VALUES (3, 	2, 	60);
INSERT INTO ambulatorio VALUES (4, 	2, 	30);
INSERT INTO ambulatorio VALUES (5, 	3, 	50);
INSERT INTO ambulatorio VALUES (6, 	3,	90);

INSERT INTO medico     	VALUES (04, 	'Joe', 		45, 'Curitiba', 		'Ortopedista', 				0);
INSERT INTO medico     	VALUES (89, 	'Homer', 	29, 'Springfield', 		'Pediatra', 				1);
INSERT INTO medico     	VALUES (46, 	'Ednaldo', 	35, 'Gheto', 			'Odontologista', 			2);
INSERT INTO medico   	VALUES (18, 	'Pereira', 	89, 'Varsóvia', 		'Cirurgião plástico', 		3);
INSERT INTO medico   	VALUES (10, 	'Subzero', 	67, 'Jacarézinho', 		'Otorrinolaringologista', 	4);
INSERT INTO medico   	VALUES (79, 	'Ayanami', 	14, 'NERV HQ', 			'Ortopedista', 				5);
INSERT INTO medico    	VALUES (90, 	'Blanka', 	52, 'Manaus', 			'Ortopedista', 				6);

INSERT INTO paciente 	VALUES (123, 	'Sakura', 	16, 'Tokyo', 			'Sarampo'					);
INSERT INTO paciente 	VALUES (345, 	'Gendo', 	54, 'Honululu', 		'Peste negra'				);
INSERT INTO paciente 	VALUES (122, 	'Langley', 	99, 'Buenos Aires', 	'HIV'						);
INSERT INTO paciente 	VALUES (876, 	'Scorpion', 66, 'Ilha da Macaca', 	'Atrofia muscular'			);
INSERT INTO paciente 	VALUES (725, 	'Ryu', 		64, 'Osaka', 			'Não utiliza caixa alta'	);
INSERT INTO paciente 	VALUES (899, 	'Ken', 		56, 'Palmeiras', 		'Sarampo'					);
INSERT INTO paciente 	VALUES (666, 	'Juuza', 	22, 'Southern Cross', 	'Herpes'					);

INSERT INTO consulta 	VALUES (04, 	122, 		'1910-01-30', 			'13:00:00'					);
INSERT INTO consulta 	VALUES (79, 	123, 		'1989-12-21', 			'12:14:00'					);
INSERT INTO consulta 	VALUES (46, 	876, 		'2000-11-14', 			'21:54:00'					);
INSERT INTO consulta 	VALUES (46, 	666, 		'1900-08-27', 			'18:12:00'					);
INSERT INTO consulta 	VALUES (46, 	899, 		'3000-09-12', 			'22:23:00'					);
INSERT INTO consulta 	VALUES (79, 	725, 		'2014-10-23', 			'11:53:00'					);
INSERT INTO consulta 	VALUES (90, 	725, 		'1800-02-26', 			'20:52:00'					);

INSERT INTO funcionario VALUES (45, 	'Mamamia', 	65, 'Manaus', 			1234.99						);
INSERT INTO funcionario VALUES (03, 	'Carl', 	65, 'Rio Branco', 		7238.93						);
INSERT INTO funcionario VALUES (78, 	'Sagan', 	65, 'Uruguaiana', 		2214.96						);
INSERT INTO funcionario VALUES (99, 	'Tony', 	65, 'Kekstain City', 	8196.99						);
INSERT INTO funcionario VALUES (12, 	'Hawks', 	65, 'Bento Cunha', 		1651.93						);
INSERT INTO funcionario VALUES (65, 	'Tatsu', 	65, 'Trevosa', 			9582.96						);
INSERT INTO funcionario VALUES (77, 	'Pacman', 	65, 'Cremosa', 			6124.99						);


#1) buscar os dados dos pacientes que estão com sarampo
SELECT *
FROM paciente
WHERE 
	doenca = 'Sarampo';
#2) buscar os dados dos médicos ortopedistas com mais de 40 anos
SELECT *
FROM medico
WHERE 
	(especialidade = 'Ortopedista') AND idade > 40;
#3) buscar os dados das consultas, exceto aquelas marcadas para os médicos com CRM 46 e 79
SELECT *
FROM consulta
WHERE 
	(crm != 46) AND (crm != 79);
#4) buscar os dados dos ambulatórios do quarto andar que ou tenham capacidade igual a 50 ou tenham número superior a 10
SELECT *
FROM ambulatorio
WHERE 
	(andar = 4 AND capacidade = 50) OR (andar = 4 AND numeroA > 10);
#5) buscar o nome e a especialidade de todos os médicos
SELECT nome, especialidade
FROM medico;
#6) buscar o número dos ambulatórios do terceiro andar
SELECT numeroA
FROM ambulatorio
WHERE 
	(andar = 3);
#7) buscar o CRM dos médicos e as datas das consultas para os pacientes com RG 122 e 725
SELECT crm, dia
FROM consulta
WHERE 
	(rg = 122) OR (rg = 725);
#8) buscar os números dos ambulatórios, exceto aqueles do segundo e quarto andares, que suportam mais de 50 pacientes
SELECT numeroA
FROM ambulatorio
WHERE 
	(andar != 2) AND (andar != 4) AND (capacidade > 50);
#9) buscar o nome dos médicos que têm consulta marcada e as datas das suas consultas
SELECT medico.nome, consulta.dia
FROM medico
INNER JOIN consulta
	ON medico.crm = consulta.crm;
#10) buscar o número e a capacidade dos ambulatórios do quinto andar e o nome dos médicos que atendem neles
SELECT ambulatorio.numeroA, ambulatorio.capacidade, medico.nome
FROM ambulatorio
INNER JOIN medico
	ON ambulatorio.numeroA = medico.numeroA
WHERE (ambulatorio.numeroA = 5 AND medico.numeroA = 5);
#11) buscar o nome dos médicos e o nome dos seus pacientes com consulta marcada, assim como a data destas consultas
SELECT medico.nome, paciente.nome, consulta.dia
FROM medico
INNER JOIN consulta
	ON medico.crm = consulta.crm
INNER JOIN paciente
	ON paciente.rg = consulta.rg;
#12) buscar os nomes dos médicos ortopedistas com consultas marcadas para o período da manhã (7hs-12hs) do dia 15/04/03
SELECT medico.nome
FROM medico
INNER JOIN consulta
	ON (medico.crm = consulta.crm) AND (medico.especialidade = 'Ortopedista')
WHERE 
	(consulta.hora BETWEEN '07:00:00' AND '12:00:00');
#13) buscar os nomes dos pacientes, com consultas marcadas para os médicos Ayanami ou Maria Souza, que estão com Sarampo
SELECT paciente.nome
FROM paciente
INNER JOIN consulta
	ON paciente.rg = consulta.rg
INNER JOIN medico
	ON consulta.crm = medico.crm
WHERE 
	(
		(medico.nome = 'Ayanami' OR medico.nome = 'Maria Souza') 
		AND 
		doenca = 'Sarampo'
    );
#14) buscar os nomes dos médicos e pacientes cadastrados no hospital
SELECT nome
FROM medico 
UNION 
SELECT nome
FROM paciente;
#15) buscar os nomes e idade dos médicos, pacientes e funcionários que residem em Florianópolis
SELECT nome, idade
FROM medico
WHERE cidade = 'Manaus'
UNION
SELECT nome, idade
FROM paciente
WHERE cidade = 'Manaus'
UNION
SELECT nome, idade
FROM funcionario
WHERE cidade = 'Manaus';
#16) buscar os nomes e RGs dos funcionários que recebem salários abaixo de R$ 300,00 e que não estão internados como pacientes

	#to fix
    
SELECT funcionario.nome, funcionario.rg
FROM funcionario
INNER JOIN paciente
	ON paciente.rg != funcionario
WHERE (funcionario.rg != paciente.rg) AND (funcionario.salario < 9000.00);
#17) buscar os números dos ambulatórios onde nenhum médico dá atendimento
SELECT ambulatorio.numeroA
FROM ambulatorio
INNER JOIN medico
	ON medico.numeroA = ambulatorio.numeroA
WHERE ambulatorio.numeroA != medico.numeroA;
#18) buscar os nomes e RGs dos funcionários que estão internados como pacientes
SELECT funcionario.nome, funcionario.rg
FROM funcionario
INNER JOIN paciente
	ON paciente.rg = funcionario.rg;
#19) buscar os dados dos ambulatórios do quarto andar. Estes ambulatórios devem ter capacidade igual a 50 ou o número do ambulatório deve ser superior a 10
SELECT *
FROM ambulatorio
WHERE (andar = 4) AND (capacidade = 50 OR numeroA > 10);
#20) buscar os números dos ambulatórios que os médicos psiquiatras atendem
SELECT ambulatorio.numeroA
FROM ambulatorio
INNER JOIN medico
	ON medico.numeroA = ambulatorio.numeroA
WHERE medico.especialidade = 'Psiquiatra';
#21) buscar o nome e o salário dos funcionários de Florianopolis e Sao José que estão internados como pacientes e têm consulta marcada com ortopedistas

	#to fix
    
SELECT funcionario.nome, funcionario.salario
FROM funcionario
INNER JOIN paciente
	ON paciente.rg = funcionario.rg
INNER JOIN PLACEHOLDER
WHERE (funcionario.cidade = 'Florianópolis' OR funcionario.cidade = 'São José') AND (medico.especialidade = 'Ortopedista');
#22) buscar o nome dos funcionários que não são pacientes

#23) Buscar o nome dos funcionários que são pacientes
SELECT funcionario.nome
FROM funcionario
INNER JOIN paciente
	ON paciente.rg = funcionario.rg;
