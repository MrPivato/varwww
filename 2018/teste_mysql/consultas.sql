# Esse arquivo contém exemplos de consultas (SELECT) em SQL
# Execute passo-a-passo e observe os resultados

### SETUP
# criando um DB específico para a demo
create database demo_select;
use demo_select;

# criando algumas tabelas
create table pessoas (
	# observe o campo auto_increment
	id int not null auto_increment,
    nome varchar(20),
    sobrenome varchar(30) not null,
    idade int,
    cidade varchar(30),
    primary key (id)
);

# populando as tabelas (não é necessário colocar 
# os valores de campos com auto_increment)
insert into pessoas (nome, sobrenome, idade, cidade) values ('eu', 'meu sobrenome', 13, 'minha cidade');
insert into pessoas (nome, sobrenome, idade, cidade) values ('tu', 'teu sobrenome', 20, 'tua cidade');
insert into pessoas (nome, sobrenome, idade, cidade) values ('ele', 'sobrenome dele', 27, 'cidade dele');
insert into pessoas (nome, sobrenome, idade, cidade) values ('ela', 'sobrenome dela', 34, 'cidade dela');

### SELECTS BÁSICOS
# seleciona toda tabela
select * from pessoas;

# seleciona apenas algumas colunas
select nome, sobrenome from pessoas;

# seleciona e renomeia colunas (alias)
select nome as n, sobrenome as s from pessoas;

# distinct remove resultados duplicados
# no exemplo abaixo, temos duas pessoas com a mesma cidade
# ex.: em quais cidades as pessoas desse banco de dados moram?
insert into pessoas (nome, sobrenome, idade, cidade) values ('outra ela', 'outro sobrenome dela', 41, 'cidade dela');
select distinct cidade from pessoas;

# distinct 2
# observe que com distinct TODO o registro deve ser único para continuar
# qual a diferença para o exemplo anterior?
select distinct sobrenome, cidade from pessoas;

# filtros simples: WHERE
select nome
from pessoas
where cidade = 'minha cidade';

select nome
from pessoas
where idade = 34;

# repare no detalhe abaixo
select nome
from pessoas
where idade = '34';

# outros operadores para o WHERE
select nome
from pessoas
where idade > 13;

# <> ou != representam o conceito de diferente
select nome
from pessoas
where idade <> 34;

# between (existe o NOT BETWEEN também)
select nome
from pessoas
where idade between 20 and 40;

# funciona com símbolos (alfanumérico)
select nome
from pessoas
where nome between 'a' and 'f';

select nome
from pessoas
where nome between 'ela' and 'elu';

# IN
select nome
from pessoas
where idade in (13, 34);

# LIKE
# o % é um wildcard, ele subsitui qualquer string
# ou seja, qualquer nome que comece com e
select nome
from pessoas
where nome like 'e%';

# o _ é um wildcard, ele subsitui qualquer caractere
# ou seja, qualquer nome que tenha a segunda letra 'l'
select nome
from pessoas
where nome like '_l_';

# AND e OR
# importante, or pode ser usado para condições no mesmo campo
# já o AND não pode (logicamente não faz sentido)
select nome
from pessoas
where (idade between 20 and 40) and (nome like 'e%');

# ORDER BY... ASC ou DESC
select nome, idade
from pessoas
order by idade desc;

# pode usar o filtro (WHERE) junto
select nome, idade, cidade
from pessoas
where idade between 20 and 40
order by cidade asc;

# caso existam dados repetidos na coluna do order by,
# pode-se usar order by em duas colunas
select *
from pessoas
order by cidade asc, idade desc;

# LIMIT
select *
from pessoas
limit 2;

# limite de 1, começando do 2
select *
from pessoas
limit 2,1;

### AGREGADORES (Funções prontas do SQL)
# avg
select avg(idade)
from pessoas;

# avg
select avg(idade), cidade
from pessoas
group by cidade;

# count
select count(idade)
from pessoas
where idade between 20 and 40;

select count(idade), cidade
from pessoas
where idade between 10 and 50
group by cidade;

# sum
select sum(idade)
from pessoas;

# operações sobre um único elemento
select sum(idade)/4
from pessoas;

# operações sobre uma tupla (vários registros)
select idade/4 + 2
from pessoas;

# observe a tabela sem primary key:
# é possível, mas não recomendável
# além disso, a InnoDB cria uma primary key invisível mesmo assim
create table produtos
(
    produto varchar(20),
    preco int,
    produtor varchar(20)
);

insert into produtos values ('cama', 400, 'produtor1');
insert into produtos values ('mesa', 200, 'produtor2');
insert into produtos values ('cadeira', 100, 'produtor2');
insert into produtos values ('armário', 600, 'produtor1');

# group by (é muito útil com joins)
select produtor, sum(preco) as total
from produtos
group by (produtor);

# having
# observe que o where não poderia ser usado no agregado total,
# apenas nas colunas originais
select produtor, sum(preco) as total
from produtos
group by (produtor)
having total > 500;

