CREATE DATABASE aluno_netshoes_data;
USE aluno_netshoes_data;
CREATE TABLE user_data(
    ID INT,
    username VARCHAR(30),
    nome VARCHAR(100),
    sobrenome VARCHAR(100),
    cpf CHAR(14),
    rg CHAR(10),
    data_nascimento DATE,
    sexo CHAR(1),
    senha VARCHAR(200),
    credit_card VARCHAR(30),
    pais VARCHAR(80),
    estado VARCHAR(80),
    cidade VARCHAR(80),
    bairro VARCHAR(80),
    rua VARCHAR(80),
    casa INT,
    cep CHAR(9)
);
CREATE TABLE produto(
    num INT,
    tamanho FLOAT,
    modelo VARCHAR(30),
    custo FLOAT,
    nome VARCHAR(150),
    cor VARCHAR(20),
    cat VARCHAR(20),
    peso FLOAT
);
CREATE TABLE categoria(
	ID INT,
    cat VARCHAR(20)
);
#-------------------------------------------------------------------------------------------

# insert a1
INSERT INTO user_data  VALUES (1, "nissim_foobar", "Nissim", "Ourfali", "111.111.111-11",
"11.111.111", "1920-12-10", "M", "vouparaaabaleia123", "123.456.78-9", "Brasil", "RJ", 
"Rio de Janeiro", "Favela", "Augusta", "1234", "99999-000" );

SELECT * FROM user_data;

#insert a2
INSERT INTO user_data (ID, username, nome, sobrenome, cpf, rg, data_nascimento, 
sexo, senha, credit_card) VALUES (2, "maria_joao", "Maria", "Joao", "222.222.222-22",
"22.222.222", "1919-01-09", "F", "comunismonaoexiste444", "123.456.78-9");

SELECT * FROM user_data;

#insert a3
INSERT INTO user_data (ID, username, nome, sobrenome, cpf, rg, data_nascimento, senha, pais, estado, cidade, 
bairro, rua, casa, cep) VALUES (3, "steve_wonds", "Steve", "Wonder", "333.333.333-33", "33.333.333", "1800-04-17", 
"intel==back_door" ,"Brasil", "RS", "Porto Alegre", "Restinga", "Rua das casas", "1010", "44331-000" );

SELECT * FROM user_data;

#----------------------------------------------------------------------------------------------

# insert b1
INSERT INTO produto VALUES (1, 38.0, "Bota", 134.90, "Bota do Ben 10", "Verde", "bota", 1.5);

SELECT * FROM produto;

# insert b2
INSERT INTO produto (num, tamanho, custo, nome) VALUES (2, 35.7, 234.90, "Tênis pirateado");

SELECT * FROM produto;

# insert b3
INSERT INTO produto (num, modelo, custo, nome, peso) 
VALUES (3, "Chinelo", 9999.90, "Chinelo mais pirateado ainda", 189.3);

SELECT * FROM produto;

#_____________________________________________________________________________________

# insert c1
INSERT INTO categoria VALUES ("bota");

SELECT * FROM categoria;

# insert c2
INSERT INTO categoria (cat) VALUES ("tenis");

SELECT * FROM categoria;

# insert c3
INSERT INTO categoria VALUES ("chinelo");

SELECT * FROM categoria;

#______________________________________________________________________________________

# update a1
UPDATE user_data SET senha = "esse_e_meu_bar_mitzvah_9090" WHERE username = "nissim_foobar";

SELECT * FROM user_data;

# update a2
UPDATE user_data SET pais = "Cuba" WHERE ID = 2;

SELECT * FROM user_data;

#--------------------------------------------------------------------------------------------

# update b1
UPDATE produto SET nome = "Bota do Ben 10, de qualidade" WHERE cor = "Verde";

SELECT * FROM produto;

# update b2
UPDATE produto SET tamanho = 43.8 WHERE nome = "Chinelo mais pirateado ainda";

SELECT * FROM produto;

#--------------------------------------------------------------------------------------------

# update c1
UPDATE categoria SET cat = "Chinelo" WHERE cat = "chinelo";

SELECT * FROM categoria;

# update c2
UPDATE categoria SET cat = "Calçado" WHERE cat = "tenis";

SELECT * FROM categoria;

#--------------------------------------------------------------------------------------------

# delete a1

DELETE FROM user_data WHERE username = "steve_wonds";

SELECT * FROM user_data;

# delete b1

DELETE FROM produto WHERE num = 1 ;

SELECT * FROM produto;

# delete c1

DELETE FROM categoria WHERE cat = "bota";

SELECT * FROM categoria;

#_______________________________________________________________________________________________

/* Não é armazenado no banco de dados:
    HTML do site
    CSS do site
    PHP do site
    Informações sigilosas
    Senhas, (Não deveria, mas estou armazenando)
*/