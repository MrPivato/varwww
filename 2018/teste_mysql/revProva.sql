# DDL
# DML
# CREATE DATABASE database;
# USE database;
#
# CREATE TABLE t1 (a INTEGER, b CHAR(10));
# ALTER TABLE t1 RENAME t2;
# ALTER TABLE t2 MODIFY a TINYINT NOT NULL, 
# CHANGE b c CHAR(20);
# ALTER TABLE t2 DROP COLUMN c;
# DROP TABLE t2;
#
# CREATE TABLE foo(
#	a CHAR(256),
#	b VARCHAR(40) #default 'algoAq'#, 
#	c TEXT, 							#Aloca espaço para até 65535 caracteres.
#	d BLOB, 							#Aloca espaço para até 65535 bytes.
#	e BIT, 								#1 bit (0 à 1)
#	f TINYINT, 							#1 byte (0 à 256)
#	g SMALLINT, 						#bytes (0 à ~65K)
#	h MEDIUMINT, 						#bytes (0 à ~16M)
#	i INT #not null#, 					#4 bytes (0 à ~4G)
#	j BIGINT, 							#8 bytes (0 à ~18446744073709551615)
#	k DECIMAL(5, 3), 					#Número em ponto-fxo com 5 bits para representar valor antes da vírgula, e 3 bits para representar valor depois da vírgula
#	l FLOAT, 							#Número em ponto-futuante com precisão simples
#	m DOUBLE, 							#Número em ponto-futuante com precisão dupla
#	n DATE, 							#'1000-01-01'
#	o TIME, 							#'HH:MM:SS'
#	p DATETIME, 						#'1970-01-01 04:12:43' 
#	q 
#	r 
#	s 
#	t 
#	u 
#	v 
# );
#
# INSERT INTO foo ('bars') VALUES ('laranja');
# UPDATE foo SET nome = "bars" WHERE cor = "suquinho";
# DELETE FROM foo WHERE cpf = "bars";
# SELECT * FROM bars WHERE cidade = "jacarezinho";
# PRIMARY KEY (numeroA)
# FOREIGN KEY (numeroA) REFERENCES ambulatorio (numeroA)
#
#
#
#
#
#
CREATE DATABASE aluno_revProva;
USE aluno_revProva;

CREATE TABLE pessoa(

	rg BIT
    

);