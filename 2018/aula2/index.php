<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo '<p align=center>Isto é um teste Estou aprendendo a escrever textos com aspas simples!</p>';
        ?>
        <hr>
        <?php
        $palavra = "teste";
        $frase = "Isto é um $palavra";
        echo $frase;
        ?>
        <hr>
        <?php
        echo `ls –l *.html`;
        ?>
        <hr>
        <?php
        echo `dir *.html`;
        ?>
        <hr>
        <?php
        define("meunome", "Juliano");
        define("peso", 78);
        echo "O meu nome é " . meunome;
        echo "<br>";
        echo "O meu peso é " . peso . " quilos";
        ?>
        <hr>
        <?php
        $num = 5000;

//        function testa_escopo1() {
        $num += 5;
        echo $num . "<br>";
//        }

        echo $num . "<br>";
        testa_escopo1();
        ?>
        <hr>
        <?php
        $num = 5000;

        function testa_escopo1() {
            global $num;
            $num += 5;
            echo $num . "<br>";
        }

        echo $num . "<br>";
//        testa_escopo1();
        ?>
        <hr>
        <?php
        $num = 5000;

//        function testa_escopo1() {
//            echo $GLOBALS["num"] . "<br>";
//            $GLOBALS["num"] ++;
//        }

        testa_escopo1();
        echo $num;
        ?>
        <hr>
        <?php
        $x = 50;
        $y = 2.35;
        $soma = (int) $y + $x;
        echo $soma;
        ?>
        <hr>
        <?php
        $x = "tri";
//        echo "Eu sou $xcolor";
        ?>
        <hr>
        <?php
        $x = "tri";
        echo "Eu sou ${x}color";
        ?>
        <hr>
        <?php
        $x = "tri";
        echo "Eu sou " . $x . "color";
        ?>
        <hr>
        <?php

        class Teste {

            function Saudacao() {
                echo "Oi pessoal!";
            }

        }

        $objeto = new Teste; // $objeto se torna uma instância da classe Teste
        $objeto->Saudacao();
        ?>
        <hr>

    </body>
</html>
