<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $i = 0;
        $k = 0;
        while ($k < 10) {
            $i++;
            $k++;
            while ($i < 20) {
                if ($i == 10) {
                    echo "Encerrando o primeiro while...<br>";
                    break;
                    echo " Essa linha não vai ser impressa!!!";
                } elseif ($i == 15) {
                    echo "Encerrando os dois whiles...";
                    break 2;
                }
                $i++;
            }
        }
        ?>
        <hr>
        <?php
        $vetor = array(1, 3, 5, 8, 11, 12, 15, 20);
        for ($i = 0; $i < sizeof($vetor); $i++) {
            if ($vetor[$i] % 2 != 0) { // é ímpar
                continue;
            }
            $num_par = $vetor[$i];
            echo "O número $num_par é par. <br>";
        }
        ?>
    </body>
</html>
