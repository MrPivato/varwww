<?php

  class Carro {

    private $nome;
    private $cor;
    private $rodas;

    

    /**
     * Get the value of nome
     */ 
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set the value of nome
     *
     * @return  self
     */ 
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get the value of cor
     */ 
    public function getCor()
    {
        return $this->cor;
    }

    /**
     * Set the value of cor
     *
     * @return  self
     */ 
    public function setCor($cor)
    {
        $this->cor = $cor;

        return $this;
    }

    /**
     * Get the value of rodas
     */ 
    public function getRodas()
    {
        return $this->rodas;
    }

    /**
     * Set the value of rodas
     *
     * @return  self
     */ 
    public function setRodas($rodas)
    {
        $this->rodas = $rodas;

        return $this;
    }
  }

{
    $car = new Carro();

    $car->setNome('AAA');
    $car->setCor('BBB');
    $car->setRodas('CCC');

    var_dump($car);
}


?>
