<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <ul style="list-style-type:disc">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ul>
        <ul style="list-style-type:circle">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ul>
        <ul style="list-style-type:square">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ul>
        <ul style="list-style-type:none">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ul>
        <ol type="1">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ol>
        <ol type="A">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ol>
        <ol type="a">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ol>
        <ol type="a">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ol>
        <ol type="I">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ol>
        <ol type="i">
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ol>
        <ul>
            <li>Coffee</li>
            <li>Tea
                <ul>
                    <li>Black tea</li>
                    <li>Green tea</li>
                </ul>
            </li>
            <li>Milk</li>
        </ul>
        <h4>Lists inside a list:</h4>
        <ul>
            <li>Coffee</li>
            <li>Tea
                <ul>
                    <li>Black tea</li>
                    <li>Green tea
                        <ul>
                            <li>China</li>
                            <li>Africa</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>Milk</li>
        </ul>
        <hr>
        <a href="lista12.html">AAAAAAAAAAAAAAAAAAAAAAAA</a>
    </body>
</html>
