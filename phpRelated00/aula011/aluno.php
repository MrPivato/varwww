<?php

include 'animal.php';

class aluno {

    public $matricula;
    public $nome;
    public $idade;
    private $sexo;

    public function verificarstatus() {
        $status = '';
        if ($this->matricula != '123') {
            $status = 'NOK';
            return $status;
        } else {
            $status = 'OK';
            return $status;
        }
    }

    public function verificarsexo() {
        if($this->sexo == 'M'){
            echo '<br>Masculino';
        }else{
            echo '<br>Feminino';
        }
    }

}
class cachorro extends animal{
    
    public $tipo_latido;
    
    public function imprimeDados() {
        echo '<p>Cachorro</p>';
        echo "<p>Nome: {$this->nome}</p>";
        echo "<p>Idade: {$this->idade}</p>";
        echo "<p>Tipo latido: {$this->tipo_latido}</p>";
    }
}
class gato extends animal{
    
    public $tipo_miado;
    
    public function imprimeDados() {
        echo '<p>Gato</p>';
        echo "<p>Nome: {$this->nome}</p>";
        echo "<p>Idade: {$this->idade}</p>";
        echo "<p>Tipo miado: {$this->tipo_miado}</p>";
    }
}
