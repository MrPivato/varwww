<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of triangulo
 *
 * @author mauricio.rosito
 */
class Triangulo {
    private $base;
    private $altura;
    public function setBase($base) {
        $this->base = $base;
    }

    public function setAltura($altura) {
        $this->altura = $altura;
    }

    public function calculaArea(){
       return ($this->base * $this->altura)/2;
    }

}
