<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of losango
 *
 * @author mauricio.rosito
 */
class Losango {
    private $diagonalMaior;
    private $diagonalMenor;
    
    public function setDiagonalMaior($diagonalMaior) {
        $this->diagonalMaior = $diagonalMaior;
    }

    public function setDiagonalMenor($diagonalMenor) {
        $this->diagonalMenor = $diagonalMenor;
    }

    public function calculaArea(){
       return ($this->diagonalMaior * $this->diagonalMenor)/2;
    }

}
