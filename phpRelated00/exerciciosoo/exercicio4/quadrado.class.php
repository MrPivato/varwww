<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of quadrado
 *
 * @author mauricio.rosito
 */
class Quadrado {
    private $lado;
    
    public function setLado($lado) {
        $this->lado = $lado;
    }

    public function calculaArea(){
       return $this->lado * $this->lado;
    }

}
