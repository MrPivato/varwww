<!DOCTYPE html>
<html>
<head>
	<title>pe2</title>
</head>
<body>
<?php
$a = 0;
$b = 1;
$p = 0;
$limit = 4000000;

for($i=0; $p < $limit; $i++) {
    $sum = $a+$b;
    $a = $b;
    $b = $sum;

    // Checks if $a is a multiple of 2
    if($a%2 == 0) {
        $p += $a;
    }
}
echo $p;

?>
</body>
</html>