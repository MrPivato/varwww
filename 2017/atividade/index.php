<!DOCTYPE html>
<html>
<head>
	<title>Atividades</title>
</head>
<body>
<!-- Dupla: Richard e Gabriel -->
	<div>
		<h1>Atividade 1</h1>
		<?php 
			for ($i=0,$x=20; $i < $x; $i++) { 
				if ($i%2==0) {
					echo $i . ",&nbsp;";
				}
			}

		?>
	</div>
	<div>
		<h1>Atividade 2</h1>
		<?php
			$x= 8;
			$y= 10;
			$z=$x;
			$x=$y;
			$y=$z;
			echo "$x";
			echo " ";
			echo "$y";
			echo "<br> ";
		?>

	</div>
	<div>
		<h1>Atividade 3</h1>
		<?php 
			$base = 10;
			$altura = 25;
			echo $base * $altura / 2;

		?>
	</div>
	<div>
		<h1>Atividade 4</h1>
		<?php 
			$a = 3;
			$b = 4;
			$c = 5;
			if (($a + $b > $c) && ($c + $b > $a) && ($a + $c > $b)) {
				echo "É um triângulo válido:<br>";
				if ($a <> $b && $b <> $c) {
					echo "É escaleno.";
				}elseif ($a == $b && $b == $c) {
					echo "É equilátero.";
				}else{
					echo "É isóceles";
				}
			}else{
				echo "Não é um triângulo";
			}
		?>
	</div>
	<div>
		<h1>Atividade 5</h1>
		<?php
			$x= array(2,300,5,1,17,145,3);
			$m=0;
			for($i=0;$i < 7;$i++){
				if($x[$i]>$m){
					$m=$x[$i];	
				}
			}
			echo "$m";
			echo "<br>";
		?>	
	</div>
	<div>
		<h1>Atividade 6</h1>
		<?php 
			$vetor= array(2,300,5,1,17,145,3);
			echo "Vetor:(";
			for ($i=0; $i < count($vetor) ; $i++) {
				echo $vetor[$i] . ",&nbsp;";
			}
			echo ")<br>";
			$x = $vetor[0];
			$y = $vetor[0];
			for ($i=0; $i < count($vetor) ; $i++) { 
				if (($x % 2 == 0) && $x < $vetor[$i]) {
					$x = $vetor[$i];
				}
				if (($y % 2 != 0) && $y > $vetor[$i]) {
					$y = $vetor[$i];
				}
			}
			echo $x + $y;
		?>
	</div>
	<div>
		<h1>Atividade 7</h1>
		<?php
			$x=12;
			$c=0;
			for($i=1; $i<=$x;$i++){
				if($x % $i ==0){
					$c++;
				}
			}
				if($c==2){
					echo "Primo";
				}else{
					echo "Não é primo";
				}
		?>	
	</div>
</body>
</html>