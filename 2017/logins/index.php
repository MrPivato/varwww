<?php
  if(isset($_POST['logar'])){
    $login = $_POST['login'];
    $senha = $_POST['senha'];

    if($login == "ban" && $senha == "ban"){
      session_start();
      $_SESSION['log'] = "Logado";
      header("Location: logado.php");
    }else{
      echo "<script>alert('Usuário ou senha incorretos');</script>";
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container" style="margin-top: 9%;">
      <form method="post" action="index.php">
        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-4">
            <label for="login">Login</label>
            <input type="text" id="login" class="form-control" name="login">
          </div>
          <div class="col-md-4">
          </div>
        </div>
        <br/>
        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-4">
            <label for="senha">Senha</label>
            <input type="password" id="senha" class="form-control" name="senha">
          </div>
          <div class="col-md-4">
          </div>
        </div>
        <br/>
        <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-4">
            <button type="submit" name="logar" class="btn btn-info btn-block">Logar</button>
          </div>
          <div class="col-md-4">
          </div>
        </div>
      </form>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
