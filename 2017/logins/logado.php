<?php
  session_start();
  if(!isset($_SESSION['log']) == "Logado"){
    header("Location: index.php");
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <script src="https://use.fontawesome.com/bbfb1deac6.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      body{
        overflow: hidden;
      }
    </style>
  </head>
  <body>
    <div class="container" style="margin-top: 9%;">
      <h2 align="center">Logado</h2>
    </div>
    <br/>
    <div class="row">
      <div class="col-md-12 fa fa-spin">
        <marquee behavior="alternate" scrollamount="123">
          <img src="https://p2.trrsf.com/image/fget/cf/fit-in/600/400/images.terra.com/2013/10/12/01presidentedilmarousseffdiadacriancainstagramrep.jpg">
        </marquee>
      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>