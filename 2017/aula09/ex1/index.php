<html>
<head>
	<title>EX_1</title>
</head>
<body>
	<h1>Exercício 1</h1>
	<hr>
	<?php 
		function criavetor(){
			//cria um vetor a ser recebido pela outra função, (criada a fim de tornar o algoritmo mais curto)
			for ($i=0; $i <= 10 ; $i++) { 
				$vetor[rand(1,2000)] = rand(1,100);
			}
			return $vetor;
		}
		function index($vetor){
			echo var_dump($vetor) . "<br><br>";
			foreach ($vetor as $key => $value) {
				if (!empty($vetor[$key])) {
					$index[] = $key;
				}
			}
			//--------------------------
			echo "<b>Índice(s):";
			for ($i=0; $i < count($index); $i++) { 
				echo $index[$i] . ",&nbsp;";
			}
			echo "</b><br><br>";
			echo var_dump($index) . "<br><br>";
		}
		//--------------------------
		echo index(criavetor());
		echo "<hr>";
		//--------------------------
		echo index(criavetor());
		echo "<hr>";
		//--------------------------
		echo index(criavetor());
		echo "<hr>";
	?>

</body>
</html>