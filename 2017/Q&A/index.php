<?php
	$gabarito = array("b","c","a","d","a","b","b","d","a","c");
	$i = 0;
	$acertos = 0;
?>
<?php
	if (count($_POST)==11) {
		global $respostas;
		foreach ($_POST as $kek) {
			if ($kek!="Enviar") {
				$respostas[$i] = $kek;
				$i++;
			}
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Perguntas</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="perguntas">
		<form method="post" action="index.php" name="perguntas">
			<legend>
				<h1>Equilibre as equações químicas:</h1>
				<p>(Substitua os valores de "X").</p>
				<p class="<?php if(!empty($_POST)) {if ($_POST!=11) {echo ' error';}}?>">(Responda todas de uma vez).</p>
				<hr>
			</legend>
			<?php $i=0;?>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<label ="nsei">
				<h3>1:XC<sub>2</sub>XH<sub>6</sub>XO  +  XO<sub>2</sub>	-->	XCO<sub>2</sub>  +  XH<sub>2</sub>O</h3>
				<input type="radio" name="0" value="a" />4, 11, 2, 8
				<br/>
				<input type="radio" name="0" value="b" />1, 3, 2, 3
				<br/>
				<input type="radio" name="0" value="c" />1, 6, 7, 9
				<br/>
				<input type="radio" name="0" value="d" />8, 1, 4, 3
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			<?php $i++?>
			<hr>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<h3>2:)  XNa<sub>2</sub>CO<sub>3</sub>  +  XHCl	--> XNaCl  +  XH<sub>2</sub>O  +  XCO<sub>2</sub></h3>
				<input type="radio" name="1" value="a" />1, 6, 7, 9
				<br/>
				<input type="radio" name="1" value="b" />1, 3, 2, 3
				<br/>
				<input type="radio" name="1" value="c" />1, 2, 2, 1, 1
				<br/>
				<input type="radio" name="1" value="d" />4, 11, 2, 8
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			<?php $i++?>
			<hr>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<h3>3:XC<sub>6</sub>H<sub>12</sub>O<sub>6</sub> --> XC<sub>2</sub>H<sub>6</sub>O  +  XCO<sub>2</sub></h3>
				<input type="radio" name="2" value="a" />1, 2, 2
				<br/>
				<input type="radio" name="2" value="b" />8, 1, 4, 3
				<br/>
				<input type="radio" name="2" value="c" />4, 11, 2, 8
				<br/>
				<input type="radio" name="2" value="d" />1, 2, 1
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			<?php $i++?>
			<hr>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<h3>4:XC<sub>4</sub>H<sub>10</sub>  +  XO<sub>2</sub> --> XCO<sub>2</sub>  +  XH<sub>2</sub>O</h3>
				<input type="radio" name="3" value="a" />4, 11, 2, 8
				<br/>
				<input type="radio" name="3" value="b" />1, 6, 7, 9
				<br/>
				<input type="radio" name="3" value="c" />1, 3, 2, 3
				<br/>
				<input type="radio" name="3" value="d" />2, 13, 8, 10
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			<?php $i++?>
			<hr>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<h3>5:XFeCl<sub>3</sub>  +  XNa<sub>2</sub>CO<sub>3</sub> --> XFe2(CO<sub>3</sub>)<sub>3</sub>  +  XNaCl</h3>
				<input type="radio" name="4" value="a" />2, 3, 1, 6
				<br/>
				<input type="radio" name="4" value="b" />4, 11, 2, 8
				<br/>
				<input type="radio" name="4" value="c" />1, 6, 7, 9
				<br/>
				<input type="radio" name="4" value="d" />1, 3, 2, 3
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			<?php $i++?>
			<hr>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<h3>6:XNH<sub>4</sub>Cl  +  XBa(OH)<sub>2</sub>	--> XBaCl<sub>2</sub>  +  XNH<sub>3</sub>  +  XH<sub>2</sub>O</h3>
				<input type="radio" name="5" value="a" />1, 6, 7, 9
				<br/>
				<input type="radio" name="5" value="b" />2, 1, 1, 2, 2
				<br/>
				<input type="radio" name="5" value="c" />1, 3, 2, 3
				<br/>
				<input type="radio" name="5" value="d" />4, 11, 2, 8
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			<?php $i++?>
			<hr>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<h3>7:XCa(OH)<sub>2</sub>  +  XH<sub>3</sub>PO<sub>4</sub>	-->	  XCa<sub>3</sub>(PO<sub>4</sub>)<sub>2</sub>  +  XH<sub>2</sub>O</h3>
				<input type="radio" name="6" value="a" />1, 3, 2, 3
				<br/>
				<input type="radio" name="6" value="b" />3, 2, 1, 6
				<br/>
				<input type="radio" name="6" value="c" />4, 11, 2, 8
				<br/>
				<input type="radio" name="6" value="d" />1, 6, 7, 9
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			<?php $i++?>
			<hr>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<h3>8:XFe<sub>2</sub>(CO<sub>3</sub>)<sub>3</sub>  +  XH<sub>2</sub>SO<sub>4</sub>	--> XFe<sub>2</sub>(SO4)<sub>3</sub>  +  XH<sub>2</sub>O  +  XCO<sub>2</sub></h3>
				<input type="radio" name="7" value="a" />1, 6, 7, 9
				<br/>
				<input type="radio" name="7" value="b" />1, 3, 2, 3
				<br/>
				<input type="radio" name="7" value="c" />4, 11, 2, 8
				<br/>
				<input type="radio" name="7" value="d" />1, 3, 1, 3, 3
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			<?php $i++?>
			<hr>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<h3>9: XNa<sub>2</sub>O  +  X(NH<sub>4</sub>)<sub>2</sub>SO<sub>4</sub>	--> XNa<sub>2</sub>SO<sub>4</sub>  +  XH<sub>2</sub>O  +  XNH<sub>3</sub></h3>
				<input type="radio" name="8" value="a" />1, 1, 1, 1, 2
				<br/>
				<input type="radio" name="8" value="b" />4, 11, 2, 8
				<br/>
				<input type="radio" name="8" value="c" />1, 3, 2, 3
				<br/>
				<input type="radio" name="8" value="d" />1, 6, 7, 9
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			<?php $i++?>
			<hr>
			<div class="pergunta <?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?>">
				<h3>10:XFeS<sub>2</sub>  +  XO<sub>2</sub>	-->	  XFe<sub>2</sub>O<sub>3</sub>  +  XSO<sub>2</sub></h3>
				<input type="radio" name="9" value="a" />1, 3, 2, 3
				<br/>
				<input type="radio" name="9" value="b" />1, 6, 7, 9
				<br/>
				<input type="radio" name="9" value="c" />4, 11, 2, 8
				<br/>
				<input type="radio" name="9" value="d" />8, 1, 4, 3
				<p><?php if (!empty($_POST)):?><?php if(!empty($respostas[$i])):?><?php if($respostas[$i]==$gabarito[$i]):?> Certo <?php $acertos+=1 ?><?php elseif($respostas[$i]!=$gabarito[$i]):?> Errado <?php endif;endif;endif?></p>
			</div>
			</label>
			<hr>
			<?php
				if (count($_POST)==11) {
						echo "<b>Você acertou:</b> $acertos pergunta(s)<hr>";
				}
			?>
			<br>
			<input type="submit" name="submit" value="Enviar">
		</form>
	</div>
</body>
</html>
