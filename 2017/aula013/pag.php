<html>
<head>
	<title>Tabela</title>
	<style type="text/css">
		table{
			margin:auto;
		}
	</style>
</head>
<body>
	<table border="1">
		<tr>
			<th>Time</th>
			<th>Pontos</th>
			<th>Jogos</th>
			<th>Vitórias</th>
			<th>Empates</th>
			<th>Derrotas</th>
			<th>Gols Pro</th>
			<th>Gols Contra</th>
			<th>Saldo de gols</th>
		</tr>
		<tr>
			<td><?php echo $_POST['nome'];  ?></td>
			<td><?php echo 3 * $_POST['v'] + $_POST['e'];  ?></td>
			<td><?php echo $_POST['v'] + $_POST['e'] + $_POST['d'];  ?></td>
			<td><?php echo $_POST['v'];  ?></td>
			<td><?php echo $_POST['e'];  ?></td>
			<td><?php echo $_POST['d'];  ?></td>
			<td><?php echo $_POST['gp'];  ?></td>
			<td><?php echo $_POST['gc'];  ?></td>
			<td><?php echo $_POST['gp'] - $_POST['gc'];  ?></td>
		</tr>
		<tr>
			<td><?php echo $_POST['nomea'];  ?></td>
			<td><?php echo 3 * $_POST['va'] + $_POST['ea'];  ?></td>
			<td><?php echo $_POST['va'] + $_POST['ea'] + $_POST['da'];  ?></td>
			<td><?php echo $_POST['va'];  ?></td>
			<td><?php echo $_POST['ea'];  ?></td>
			<td><?php echo $_POST['da'];  ?></td>
			<td><?php echo $_POST['gpa'];  ?></td>
			<td><?php echo $_POST['gca'];  ?></td>
			<td><?php echo $_POST['gpa'] - $_POST['gca'];  ?></td>
		</tr>
		<tr>
			<td><?php echo $_POST['nomeb'];  ?></td>
			<td><?php echo 3 * $_POST['vb'] + $_POST['eb'];  ?></td>
			<td><?php echo $_POST['vb'] + $_POST['eb'] + $_POST['db'];  ?></td>
			<td><?php echo $_POST['vb'];  ?></td>
			<td><?php echo $_POST['eb'];  ?></td>
			<td><?php echo $_POST['db'];  ?></td>
			<td><?php echo $_POST['gpb'];  ?></td>
			<td><?php echo $_POST['gcb'];  ?></td>
			<td><?php echo $_POST['gpb'] - $_POST['gcb'];  ?></td>
		</tr>
		<tr>
			<td><?php echo $_POST['nomec'];  ?></td>
			<td><?php echo 3 * $_POST['vc'] + $_POST['ec'];  ?></td>
			<td><?php echo $_POST['vc'] + $_POST['ec'] + $_POST['dc'];  ?></td>
			<td><?php echo $_POST['vc'];  ?></td>
			<td><?php echo $_POST['ec'];  ?></td>
			<td><?php echo $_POST['dc'];  ?></td>
			<td><?php echo $_POST['gpc'];  ?></td>
			<td><?php echo $_POST['gcc'];  ?></td>
			<td><?php echo $_POST['gpc'] - $_POST['gcc'];  ?></td>
		</tr>
		<tr>
			<td><?php echo $_POST['nomed'];  ?></td>
			<td><?php echo 3 * $_POST['vd'] + $_POST['ed'];  ?></td>
			<td><?php echo $_POST['vd'] + $_POST['ed'] + $_POST['dd'];  ?></td>
			<td><?php echo $_POST['vd'];  ?></td>
			<td><?php echo $_POST['ed'];  ?></td>
			<td><?php echo $_POST['dd'];  ?></td>
			<td><?php echo $_POST['gpd'];  ?></td>
			<td><?php echo $_POST['gcd'];  ?></td>
			<td><?php echo $_POST['gpd'] - $_POST['gcd'];  ?></td>
		</tr>
	</table>
	<button><a href="index.html">VOLTAR</a></button>
</body>
</html>