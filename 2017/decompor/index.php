<!DOCTYPE html>
<html>
<head>
	<title>Index</title>
</head>
<body>
	<form action="index.php" method="post">

		<fieldset>

			<legend><h1>Digite o num para decompor:</h1></legend>
			<input type="number" name="num">
			<input type="submit" name="submit" value="Calcular">

		</fieldset>

	</form>

	<?php

		if (!empty($_POST)) {

			echo '<h1>'.$_POST['num'].':</h1>';

			for ($i=1; $i <= $_POST['num']; $i++) { 
				
				if (($_POST['num'] % $i) == 0) {

					echo $i . '<br>';
				}
			}
		}

	?>
</body>
</html>
